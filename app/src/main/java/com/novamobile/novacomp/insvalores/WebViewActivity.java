package com.novamobile.novacomp.insvalores;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class WebViewActivity extends NavbarUtils {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        super.fntNavbarfunctions(this,true,true);

        Intent intent = getIntent();

        LoadWebViewTask objTask = new LoadWebViewTask();
        objTask.execute(intent.getIntExtra("Opcion",0));
    }

    public class LoadWebViewTask extends AsyncTask<Integer, Integer, String> {



        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Integer... params) {

            return fntUrl(params[0]);
        }

        @Override
        protected void onPostExecute(final String resultado) {
            WebView browser =(WebView)findViewById(R.id.WebBoletines);
            browser.getSettings().setJavaScriptEnabled(true);
            browser.getSettings().setLoadWithOverviewMode(true);
            browser.getSettings().setUseWideViewPort(true);
            browser.getSettings().setBuiltInZoomControls(true);
            /*browser.setWebViewClient(new WebViewClient(){

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    view.loadUrl(url);
                    return true;
                }
            });*/

            browser.loadUrl(resultado);
        }

        private String fntUrl(int pOpcion){

            String Url = "";

            switch (pOpcion){
                case 1 :
                    Url = "https://www.insvalores.com/WSINSValoresPublico/Archivos%20Pagina/25-Boletines%20Diarios-Mercado/25-2017-Boletin%20diario%20local%20e%20int_060417.pdf";
                    break;
                case 2 :
                    Url = "https://www.insvalores.com/WSINSValoresPublico/Archivos%20Pagina/26-Boletines%20Semanales/26-2017-14-Boletin%20semanal%20del%2003%20al%2007%20de%20abril%20del%202017.pdf";
                    break;
                case 3 :
                    Url = "https://www.insvalores.com/WSINSValoresPublico/Archivos%20Pagina/31-Boletines%20Mensuales/31-2017-03-Boletin%20Mensual%20Ins%20Valores%20Marzo.pdf";
                    break;
            }


            return "https://docs.google.com/gview?embedded=true&url=" + Url;
        }
    }
}
