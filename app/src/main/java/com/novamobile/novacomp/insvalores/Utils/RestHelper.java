package com.novamobile.novacomp.insvalores.Utils;

import android.os.StrictMode;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RestHelper {


    private HttpURLConnection conn;
    private URL url;
    private String HttpMethod;
    private String urlParameters = "";
    private String UrlMethod;
    private int    postDataLength;
    private JSONObject parametros = new JSONObject();

    public RestHelper(String pHttpMethod,String pUrlMethod){
        this.UrlMethod = pUrlMethod;
        this.HttpMethod = pHttpMethod;
    }

    public void fntSetParametersPost(String pKey,Object pValue){
        try{
            parametros.put(pKey,pValue);
        }catch (JSONException ex){
            Log.e("ERROR",ex.getMessage());
        }
    }

    public void fntSetParametersPost(JSONObject pValue){
        parametros = pValue;
    }

    private void fntAbrirConn() {
        try {
            URL url = new URL(UrlMethod);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty( "Accept", "application/json");
            conn.setRequestMethod(HttpMethod);
            if(HttpMethod=="POST"){
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
            }
            conn.connect();
        } catch (ProtocolException Proe) {
            Proe.printStackTrace();
        } catch (IOException IOe) {
            IOe.printStackTrace();
        }

    }

    public JSONObject fntGetDataURL() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        BufferedReader br = null;
        JSONObject JsonResponse = null;
        try {
            fntAbrirConn();



            if( parametros.length()>0) {
                //DataOutputStream writer;

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(parametros.toString());

                //writer = new DataOutputStream(conn.getOutputStream ());
                //writer.writeBytes(URLEncoder.encode(parametros.toString(),"UTF-8"));

                writer.flush();
                writer.close ();
            }

            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                try {
                    JsonResponse = new JSONObject(sb.toString());
                    //JSONObject response = (JSONObject) new JSONTokener(JsonResponse.toString()).nextValue();

                } catch (JSONException ex) {
                    System.out.println(ex.getMessage());
                }

                br.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return JsonResponse;
    }


}
