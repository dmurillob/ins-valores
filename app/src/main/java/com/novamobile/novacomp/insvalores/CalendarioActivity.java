package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.marcohc.robotocalendar.RobotoCalendarView;
import com.novamobile.novacomp.insvalores.Adapter.AdapterListaVencimiento;
import com.novamobile.novacomp.insvalores.Entity.VencimientoEntity;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;
import com.novamobile.novacomp.insvalores.Utils.OnSwipeTouchListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CalendarioActivity extends NavbarUtils implements RobotoCalendarView.RobotoCalendarListener {


    private int mRangeMonths = 3;
    private Activity mActivity;
    private RobotoCalendarView robotoCalendarView;
    private ListView mListVencimiento;
    private LinearLayout mlayoutListaVencimientos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);
        super.fntNavbarfunctions(this,false,true);
        mActivity = this;

        TextView txtNombre = (TextView)findViewById(R.id.txtNombre);
        Button btnFechaActual = (Button)findViewById(R.id.btnFechaActual);
        mListVencimiento = (ListView)findViewById(R.id.ListVencimientos);
        mlayoutListaVencimientos = (LinearLayout)findViewById(R.id.layoutListaVencimientos);

        btnFechaActual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date today = new Date();
                Calendar CalendarToday = Calendar.getInstance();
                CalendarToday.setTime(today);

                robotoCalendarView.setCalendar(CalendarToday);
                robotoCalendarView.updateView();

                mListVencimiento.setAdapter(null);

                mlayoutListaVencimientos.setVisibility(View.GONE);

            }
        });

        txtNombre.setText("Fechas y vencimientos");

        Date today = new Date();
        robotoCalendarView = (RobotoCalendarView)findViewById(R.id.robotoCalendarPicker);
        robotoCalendarView.setRobotoCalendarListener(this);
        robotoCalendarView.setShortWeekDays(false);
        robotoCalendarView.showDateTitle(true);
        robotoCalendarView.setHovered(true);
        robotoCalendarView.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.FondoBlanco));
        robotoCalendarView.updateView();

        Calendar CalendarToday = Calendar.getInstance();
        CalendarToday.add(Calendar.DAY_OF_MONTH, -5);
        robotoCalendarView.markCircleImage2(CalendarToday);
        //robotoCalendarView.markCircleImage1(CalendarToday);
        //robotoCalendarView.markCircleImage1(CalendarToday);
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onDayClick(Calendar daySelectedCalendar) {


        mListVencimiento.setAdapter(null);
        mlayoutListaVencimientos.setVisibility(View.VISIBLE);

        ArrayList<VencimientoEntity> ListEntity = new  ArrayList<VencimientoEntity>();

        final TextView txtMontoVencimiento = (TextView)findViewById(R.id.txtMontoVencimiento);
        txtMontoVencimiento.setText("$ 123,456,789");

        for(int i = 0 ; i <= 2; i++){

            VencimientoEntity objEntity = new VencimientoEntity();
            objEntity.setmIdVencimiento(i);
            objEntity.setmVencimientoDesc("Registro : " + String.valueOf(i));

            ListEntity.add(objEntity);
        }

        AdapterListaVencimiento adapter = new AdapterListaVencimiento(mActivity.getApplicationContext(),android.R.layout.simple_list_item_1,ListEntity,mActivity);
        mListVencimiento.setAdapter(adapter);
        //Toast.makeText(this, "onDayClick: " + daySelectedCalendar.getTime(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDayLongClick(Calendar daySelectedCalendar) {
        //Toast.makeText(this, "onDayLongClick: " + daySelectedCalendar.getTime(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRightButtonClick() {
        mlayoutListaVencimientos.setVisibility(View.GONE);
        mListVencimiento.setAdapter(null);
        //Toast.makeText(this, "onRightButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeftButtonClick() {
        mlayoutListaVencimientos.setVisibility(View.GONE);
        mListVencimiento.setAdapter(null);
        //Toast.makeText(this, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
    }


}
