package com.novamobile.novacomp.insvalores.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.PublicActivity;
import com.novamobile.novacomp.insvalores.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;


public class GeneralUtils {

    public static void fntShowDialog(final Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public static void fntShowDialogClose(final Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });


        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(activity,PublicActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
            }
        });
        builder.show();
    }

    public static void fntAsesorEmail(Activity pActivity){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {});
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        pActivity.startActivity(Intent.createChooser(intent,"Send"));
    }


    public static void fntLostFocus(EditText pEditTxt,Activity pActivity){

        if(pEditTxt!=null)
            pEditTxt.clearFocus();

        View view = pActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)pActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void fntFillLayout(LinearLayout layout, JSONArray json, String colorTitulo, boolean punto) {
        for (int i = 0; i < json.length(); i++)
        {
            try
            {
                JSONObject obj = json.getJSONObject(i);

                fntFillLayout(layout, obj, colorTitulo, punto);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public static void fntFillLayout(LinearLayout layout, JSONObject json, String colorTitulo, boolean punto) {
        LayoutInflater inflater = LayoutInflater.from(layout.getContext());

        LinearLayout item = (LinearLayout) inflater.inflate(R.layout.celda_titulo_valor, null);

        ImageView imgColor = (ImageView) item.findViewById(R.id.imgColor);
        TextView lblTitulo = (TextView) item.findViewById(R.id.lblTitulo);
        TextView lblValor = (TextView) item.findViewById(R.id.lblValor);

        String titulo = null, color = null;

        try {
            if (json.has("Jerarquia") && json.getInt("Jerarquia") == 1) {
                lblTitulo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                if (colorTitulo != null)
                {
                    lblTitulo.setTextColor(Color.parseColor(GeneralUtils.fntGetHexColor(colorTitulo)));
                }
            }

            if (json.has("Titulo") && !json.getString("Titulo").trim().equals("")) {
                lblTitulo.setTypeface(null, Typeface.BOLD);

                titulo = json.getString("Titulo");
            } else if (json.has("Nombre") && !json.getString("Nombre").trim().equals("")) {
                titulo = json.getString("Nombre");
            }
            else
            {
                item.setVisibility(View.GONE);
            }

            if (!punto || titulo == null) {
                imgColor.setVisibility(View.GONE);
            }

            lblTitulo.setText(titulo);

            if (json.has("Color")) {
                color = GeneralUtils.fntGetHexColor(json.getString("Color"));

                try {
                    lblTitulo.setTextColor(Color.parseColor(color));

                    if (punto) {
                        imgColor.setColorFilter(Color.parseColor(color));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (json.has("Valor") && !json.getString("Valor").trim().equals("")) {
                lblValor.setText(json.getString("Valor"));
            } else {
                lblValor.setVisibility(View.GONE);
            }

            layout.addView(item);

            if (json.has("Data")) {
                fntFillLayout(layout, json.getJSONArray("Data"), colorTitulo, punto);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void fntFillLayout(LinearLayout layout, JSONObject json) {
        fntFillLayout(layout, json, null, false);
    }

    public static void fntFillLayout(LinearLayout layout, JSONObject json, String colorTitulo) {
        fntFillLayout(layout, json, colorTitulo, false);
    }

    public static void fntFillLayout(LinearLayout layout, JSONObject json, boolean punto) {
        fntFillLayout(layout, json, null, punto);
    }

    public static void fntFillLayout(LinearLayout layout, JSONArray json) {
        fntFillLayout(layout, json, null, false);
    }

    public static void fntFillLayout(LinearLayout layout, JSONArray json, String colorTitulo) {
        fntFillLayout(layout, json, colorTitulo, false);
    }

    public static void fntFillLayout(LinearLayout layout, JSONArray json, boolean punto) {
        fntFillLayout(layout, json, null, punto);
    }

    public static String fntGetHexColor(String color) {
        if (!color.contains("#"))
        {
            color = "#" + color;
        }

        return color;
    }
}
