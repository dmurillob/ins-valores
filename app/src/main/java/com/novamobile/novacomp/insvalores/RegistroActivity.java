package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegistroActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mActivity = this;

        Button btnCancelar = (Button)findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intento = new Intent(mActivity, PublicActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_right, R.anim.activity_slide_in_left);
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intento = new Intent(mActivity, PublicActivity.class);
        mActivity.startActivity(intento);
        mActivity.overridePendingTransition(R.anim.activity_slide_out_right, R.anim.activity_slide_in_left);
    }
}
