package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.novamobile.novacomp.insvalores.Adapter.AdapterListaCartera;
import com.novamobile.novacomp.insvalores.Entity.CarteraEntity;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.util.ArrayList;

public class ListaCarteraActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cartera);
        super.fntNavbarfunctions(this,false,true);

        mActivity = this;

        ArrayList<CarteraEntity> ListEntity = new ArrayList<CarteraEntity>();

        for (int i = 0 ; i<= 3 ; i++){
            CarteraEntity objEntity = new CarteraEntity();
            objEntity.setIdCartera(i);
            objEntity.setCartera("CLIENTE " + String.valueOf(i));
            ListEntity.add(objEntity);
        }

        ListAdapter adapter = new AdapterListaCartera(getApplicationContext(),android.R.layout.simple_list_item_1,ListEntity,mActivity);
        ListView Lista = (ListView)findViewById(R.id.ListItems);
        Lista.setAdapter(adapter);


    }
}
