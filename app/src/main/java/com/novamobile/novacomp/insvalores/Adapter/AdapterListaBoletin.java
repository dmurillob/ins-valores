package com.novamobile.novacomp.insvalores.Adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Entity.CarteraEntity;
import com.novamobile.novacomp.insvalores.MenuActivity;
import com.novamobile.novacomp.insvalores.PdfViewerActivity;
import com.novamobile.novacomp.insvalores.PortafolioActivity;
import com.novamobile.novacomp.insvalores.R;
import com.novamobile.novacomp.insvalores.WebViewActivity;

import java.util.ArrayList;
import java.util.List;

public class AdapterListaBoletin extends ArrayAdapter<String> {

    Context mContext;
    Activity mActivity;
    List<String> mListItems;

    public AdapterListaBoletin(Context context, int resource) {
        super(context, resource);
    }

    public AdapterListaBoletin(Context context, int resource, List<String> items, Activity activity) {
        super(context, resource, items);
        mContext=context;
        mListItems = items;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_boletin, null);
        }

        String Boletin = mListItems.get(position);

        final TextView txtboletin = (TextView)convertView.findViewById(R.id.txtboletinCelda);
        txtboletin.setText(Boletin);

        final int opcion = fntOpcion(mListItems.get(position));
        final RelativeLayout eventsLL = (RelativeLayout) convertView.findViewById(R.id.layoutCelda);
        eventsLL.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                Intent intento = new Intent(mActivity, WebViewActivity.class);
                intento.putExtra("Opcion",opcion);
                intento.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }

        });
        return convertView;
    }


    private int fntOpcion(String pOpcion){

        int Result = 0;

        switch (pOpcion){
            case "Mercado":
                Result = 1;
                break;
            case "Semanal":
                Result = 2;
                break;
            case "Mensual":
                Result = 3;
                break;
        }

        return Result;
    }

}
