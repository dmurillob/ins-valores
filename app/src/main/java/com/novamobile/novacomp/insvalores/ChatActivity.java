package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Utils.GeneralUtils;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends NavbarUtils {

    private Activity mActivity;
    private EditText mtxtEnviar;
    private LinearLayout mLayoutChat;
    private Map<String, String> hashMapDatos = new HashMap<String, String>();
    private ScrollView mScrollViewChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        super.fntNavbarfunctions(this,false,true);
        fntInitHashMap();
        mActivity = this;
        TextView txtNombre = (TextView)findViewById(R.id.txtNombre);
        mLayoutChat = (LinearLayout)findViewById(R.id.layoutChat);
        Button btnEnviar = (Button)findViewById(R.id.btnEnviar);
        Button btnAyuda = (Button)findViewById(R.id.btnAyuda);
        Button btnDemo = (Button)findViewById(R.id.btnDemo);
        Button btnAsesor = (Button)findViewById(R.id.btnAsesor);
        mtxtEnviar = (EditText)findViewById(R.id.txtEnviar);
        mScrollViewChat = (ScrollView)findViewById(R.id.ScrollViewChat);


        txtNombre.setText("Contacte a su asesor");

        btnDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Respuesta = fntGetValue(String.valueOf("Demo").toLowerCase());
                fntAddElement(2,"Demo");

                if(Respuesta != null){
                    fntAddElement(1,Respuesta);
                }else{
                    fntAddElement(1,"Comando no reconocido");
                }

                GeneralUtils.fntLostFocus(mtxtEnviar,mActivity);
                mtxtEnviar.setText("");

                mScrollViewChat.post(new Runnable() {
                    @Override
                    public void run() {
                        mScrollViewChat.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });

        btnAyuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Respuesta = fntGetValue(String.valueOf("Ayuda").toLowerCase());
                fntAddElement(2,"Ayuda");

                if(Respuesta != null){
                    fntAddElement(1,Respuesta);
                }else{
                    fntAddElement(1,"Comando no reconocido");
                }

                GeneralUtils.fntLostFocus(mtxtEnviar,mActivity);
                mtxtEnviar.setText("");

                mScrollViewChat.post(new Runnable() {
                    @Override
                    public void run() {
                        mScrollViewChat.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });

        btnAsesor.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GeneralUtils.fntAsesorEmail(mActivity);
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mtxtEnviar.getText().toString().length() > 0){

                    String Respuesta = fntGetValue(mtxtEnviar.getText().toString().toLowerCase());
                    fntAddElement(2,mtxtEnviar.getText().toString());

                    if(Respuesta != null){
                        fntAddElement(1,Respuesta);
                    }else{
                        fntAddElement(1,"Comando no reconocido");
                    }

                    GeneralUtils.fntLostFocus(mtxtEnviar,mActivity);
                    mtxtEnviar.setText("");

                    /*
                    ScrollView ScrollViewChat = (ScrollView)findViewById(R.id.ScrollViewChat);
                    ScrollViewChat.fullScroll(View.FOCUS_DOWN);
                    */


                    mScrollViewChat.post(new Runnable() {
                        @Override
                        public void run() {
                            mScrollViewChat.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }


            }
        });


        mLayoutChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralUtils.fntLostFocus(mtxtEnviar,mActivity);
            }
        });


        fntAddElement(1,"Hola, en que puedo ayudarle");

    }

    private void fntAddElement(int pType,String pMessage){

        LayoutInflater Inflatelayout = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = null;

        if(pType==1){
            layout = (LinearLayout) Inflatelayout.inflate(R.layout.row_chat_automatico, null);
        }else  if(pType==2){
            layout = (LinearLayout) Inflatelayout.inflate(R.layout.row_chat_usuario, null);
        }

        TextView txtTexto = (TextView)layout.findViewById(R.id.txtTexto);
        txtTexto.setText(pMessage);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,10,0,0);
        layout.setLayoutParams(lp);

        mLayoutChat.addView(layout);
    }

    private String fntGetValue(String pKey){
        return hashMapDatos.get(pKey);
    }


    private void fntInitHashMap(){
        hashMapDatos.put("ayuda" , "Puedes hacer uso de los siguientes comandos:\n" +
                                    "Tipo cambio dolar venta: TCDV \n" +
                                    "Tipo cambio dolar compra: TCDC");

        hashMapDatos.put("demo" , "Por ejemplo puede escribir:\n" +
                                    "\"Tipo cambio dolar venta\"");

        hashMapDatos.put("tipo cambio dolar venta" , "Hoy el dolar se vende a 555,93");
        hashMapDatos.put("tcdv" , "Hoy el dolar se vende a 555,93");
        hashMapDatos.put("tipo cambio dolar compra" , "Hoy el dolar se compra a 558,93");
        hashMapDatos.put("tcdc" , "Hoy el dolar se vende a 558,93");
    }


}
