package com.novamobile.novacomp.insvalores.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.novamobile.novacomp.insvalores.DetalleComposicionActivity;
import com.novamobile.novacomp.insvalores.GraficoPrecioMoneda;
import com.novamobile.novacomp.insvalores.MenuActivity;
import com.novamobile.novacomp.insvalores.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class fragSliceChart extends Fragment {

    private int index;
    private String titulo;

    private PieChart mChart;
    // we're going to display pie chart for smartphones martket shares


    private ArrayList<String> ArrayNames = new ArrayList<String>();
    private ArrayList<Float> ArrayValues = new ArrayList<Float>();
    private ArrayList<Integer> ArrayColors = new ArrayList<Integer>();

    private float[] yData = { 5, 10, 15, 30, 40 };
    private String[] xData = { "NASQ", "XBI", "Tokyo", "Apple", "Samsung" };


    public static fragSliceChart newInstance(int index,String Titulo) {

        // Instantiate a new fragment
        fragSliceChart fragment = new fragSliceChart();

        // Save the parameters
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        bundle.putString("titulo", Titulo);
        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);

        return fragment;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.index = (getArguments() != null) ? getArguments().getInt("index")  : -1;
        this.titulo = (getArguments() != null) ? getArguments().getString("titulo")  : "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayValues.add((float)5);
        ArrayValues.add((float)10);
        ArrayValues.add((float)15);
        ArrayValues.add((float)30);
        ArrayValues.add((float)40);

        ArrayNames.add("NASQ");
        ArrayNames.add("XBI");
        ArrayNames.add("Tokyo");
        ArrayNames.add("Dow");
        ArrayNames.add("PECL");

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_graficos,container, false);
        LinearLayout LayoutControls = (LinearLayout) rootView.findViewById(R.id.layoutControls);

        TextView txtNombre = (TextView)rootView.findViewById(R.id.txtNombre);
        txtNombre.setText(this.titulo);

        for (int i = 1; i <= ArrayNames.size(); i++) {

            if((index==3)||(index==4)) {

                LayoutInflater Inflatelayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout layout = (LinearLayout) Inflatelayout.inflate(R.layout.layout_button_chart, null);
                RelativeLayout RelativelayoutControls = (RelativeLayout)layout.findViewById(R.id.layoutControls);
                //RelativelayoutControls.setBackgroundColor(fntGetColorBotton(i));


                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.TOP_BOTTOM,
                        new int[] {fntGetColorBotton(i),fntGetColorBotton(i)});
                gd.setCornerRadius(50f);

                RelativelayoutControls.setBackgroundDrawable(gd);

                TextView txtDescripcion = (TextView) layout.findViewById(R.id.txtRubroGrafico);
                txtDescripcion.setText(ArrayNames.get(i - 1));

                final int IndexArray = i;

                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(index==3){
                            Intent intento = new Intent(getActivity(), GraficoPrecioMoneda.class);
                            //intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(intento);
                            getActivity().overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
                        }else if(index==4){
                            Intent intento = new Intent(getActivity(), DetalleComposicionActivity.class);
                            intento.putExtra("Name",ArrayNames.get(IndexArray));
                            //intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(intento);
                            getActivity().overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);

                        }
                    }
                });

                LayoutControls.addView(layout);
            }

            ArrayColors.add(fntGetColorBotton(i));
        }

        mChart = (PieChart)rootView.findViewById(R.id.PieChar);

        mChart.setUsePercentValues(true);
        // enable hole and configure
        mChart.setDrawHoleEnabled(true);
        //mChart.setHoleColorTransparent(true);
        mChart.setHoleRadius(0);
        mChart.setTransparentCircleRadius(10);

        // enable rotation of the chart by touch
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(false);



        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

                if(index==3){
                    String ValueX = String.valueOf(e.getX());
                    String ValueY = String.valueOf(e.getY());
                }

            }

            @Override
            public void onNothingSelected() {

            }
        });

        // add data
        addData();

        /*
        // customize legends
        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setXEntrySpace(7);
        l.setYEntrySpace(5);
        */

        return rootView;

    }




    private void addData() {
        List<PieEntry> yVals1 = new ArrayList<PieEntry>();

        for (int i = 0; i < yData.length; i++)
            yVals1.add(new PieEntry(ArrayValues.get(i),ArrayNames.get(i)));

            //yVals1.add(new PieEntry(yData[i], xData[i]));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < xData.length; i++)
            xVals.add(xData[i]);

        // create pie data set
        PieDataSet dataSet = new PieDataSet(yVals1,"");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);
        dataSet.setColors(ArrayColors);

        // instantiate pie data object now
        ArrayList<IPieDataSet> dataSets = new ArrayList<IPieDataSet>();

        dataSets.add(dataSet);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.GRAY);

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        // update pie chart
        mChart.invalidate();
    }




    private int fntGetColorBotton(int pColor){

        int ResultColor = 0;

        switch (pColor){
            case 1:
                ResultColor = Color.rgb(255,0,0);
                break;
            case 2:
                ResultColor = Color.rgb(45,171,233);
                break;
            case 3:
                ResultColor = Color.rgb(91,205,34);
                break;
            case 4:
                ResultColor = Color.rgb(205,199,34);
                break;
            case 5:
                ResultColor = Color.rgb(47,229,187);
                break;
            case 6:
                ResultColor = Color.rgb(229,187,47);
                break;
            case 7:
                ResultColor = Color.rgb(108,47,229);
                break;

        }

        return ResultColor;
    }

}
