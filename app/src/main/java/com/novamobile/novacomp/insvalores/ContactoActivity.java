package com.novamobile.novacomp.insvalores;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ContactoActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);

        mActivity = this;

        Button btnVolver = (Button)findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fntVolver();
            }
        });

        EditText txtWeb = (EditText)findViewById(R.id.txtWeb);
        txtWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                mActivity.startActivity(browserIntent);
            }
        });

        final EditText txtTel = (EditText)findViewById(R.id.txtTel);
        txtTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT > 22){
                    if (mActivity.getApplicationContext().checkSelfPermission(Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (mActivity.shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)) {

                        } else {
                            mActivity.requestPermissions( new String[]{Manifest.permission.CALL_PHONE}, 1);
                        }
                    }else{
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + txtTel.getText().toString()));
                        mActivity.startActivity(callIntent);
                    }
                }
            }
        });


        EditText txtUbicacion = (EditText)findViewById(R.id.txtUbicacion);
        txtUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, MapaActivity.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_in_right,R.anim.activity_slide_out_left);
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        fntVolver();
    }

    private void fntVolver(){
        Intent intento = new Intent(mActivity, PublicActivity.class);
        intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.startActivity(intento);
        mActivity.overridePendingTransition(R.anim.activity_slide_in_up,R.anim.activity_slide_out_down);
    }
}
