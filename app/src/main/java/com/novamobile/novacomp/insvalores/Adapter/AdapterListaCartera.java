package com.novamobile.novacomp.insvalores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Entity.CarteraEntity;
import com.novamobile.novacomp.insvalores.ListaCarteraActivity;
import com.novamobile.novacomp.insvalores.PortafolioActivity;
import com.novamobile.novacomp.insvalores.R;

import java.util.List;

public class AdapterListaCartera extends ArrayAdapter<CarteraEntity> {

    Context mContext;
    Activity mActivity;
    List<CarteraEntity> mListItems;

    public AdapterListaCartera(Context context, int resource) {
        super(context, resource);
    }

    public AdapterListaCartera(Context context, int resource, List<CarteraEntity> items, Activity activity) {
        super(context, resource, items);
        mContext=context;
        mListItems = items;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_cartera, null);
        }

        final CarteraEntity objEntity = mListItems.get(position);

        final TextView txtTitulo = (TextView)convertView.findViewById(R.id.txtPortafolio);
        txtTitulo.setText(objEntity.getCartera());

        final LinearLayout eventsLL = (LinearLayout) convertView.findViewById(R.id.layoutCartera);
        eventsLL.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                Intent intento = new Intent(mActivity, PortafolioActivity.class);
                intento.putExtra("Titulo", objEntity.getCartera());
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }

        });

        return convertView;
    }
}
