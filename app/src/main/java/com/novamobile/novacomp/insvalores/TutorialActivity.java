package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.novamobile.novacomp.insvalores.Adapter.CustomFragmentPagerAdapter;
import com.novamobile.novacomp.insvalores.Utils.ProgressDotWidget;
import com.novamobile.novacomp.insvalores.fragment.fragSlice;

public class TutorialActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mActivity = this;

        ViewPager pager = (ViewPager) this.findViewById(R.id.pager);
        final ProgressDotWidget objDot = (ProgressDotWidget)findViewById(R.id.progress_dots);

        objDot.setDotCount(5);

        // Create an adapter with the fragments we show on the ViewPager
        CustomFragmentPagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(fragSlice.newInstance(0) );
        adapter.addFragment(fragSlice.newInstance(1) );
        adapter.addFragment(fragSlice.newInstance(2) );
        adapter.addFragment(fragSlice.newInstance(3) );
        adapter.addFragment(fragSlice.newInstance(4) );

        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                objDot.setActivePosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        fntVolver();
    }

    private void fntVolver(){
        Intent intento = new Intent(mActivity, PublicActivity.class);
        intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.startActivity(intento);
        mActivity.overridePendingTransition(R.anim.activity_slide_in_left,R.anim.activity_slide_out_right);
    }
}
