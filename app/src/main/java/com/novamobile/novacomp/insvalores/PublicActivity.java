package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;

import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PublicActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public);


        Button btnIngresar = (Button) findViewById(R.id.btnIngresar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = null;
                intento = new Intent(mActivity, MenuActivity.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });

        mActivity = this;

        TabLayout mtabs = (TabLayout)mActivity.findViewById(R.id.tabs);
        TabLayout.Tab TabInfo, TabUbicacion, TabRegistro, TabTerminos;

        TabInfo = mtabs.newTab().setText("Información");
        TabInfo.setIcon(R.drawable.ic_smartphone_call);

        TabUbicacion = mtabs.newTab().setText("Contacto");
        TabUbicacion.setIcon(R.drawable.ic_arroba);

        TabRegistro = mtabs.newTab().setText("Registro");
        TabRegistro.setIcon(R.drawable.ic_arroba);

        TabTerminos = mtabs.newTab().setText("Terminos");
        TabTerminos.setIcon(R.drawable.ic_arroba);

        mtabs.addTab(TabInfo);
        mtabs.addTab(TabUbicacion);
        mtabs.addTab(TabRegistro);
        mtabs.addTab(TabTerminos);

        mtabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                fntOpcion(tab.getPosition());
            }

            @Override
            public void onTabSelected(TabLayout.Tab tab){
                fntOpcion(tab.getPosition());
            }
        });
    }

    private void fntOpcion(int pPosition){
        Intent intento = null;

        switch (pPosition){
            case 0:
                intento = new Intent(mActivity, TutorialActivity.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);
                break;
            case 1:
                intento = new Intent(mActivity, ContactoActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_in_down, R.anim.activity_slide_out_up);
                break;
            case 2:
                intento = new Intent(mActivity, RegistroActivity.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_in_down, R.anim.activity_slide_out_up);
                break;
            case 3:
                intento = new Intent(mActivity, TerminosActivity.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
                break;
        }
    }
}
