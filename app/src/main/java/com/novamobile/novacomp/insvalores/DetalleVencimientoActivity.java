package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Utils.GeneralUtils;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import org.json.JSONArray;

public class DetalleVencimientoActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_vencimiento);
        super.fntNavbarfunctions(this,true,true);

        mActivity = this;
        TextView txtNombre = (TextView)findViewById(R.id.txtNombre);
        txtNombre.setText("Fechas y vencimientos");

        Button btnAsesor = (Button)findViewById(R.id.btnContactorAsesor);
        btnAsesor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralUtils.fntAsesorEmail(mActivity);
            }
        });

        JSONArray data = null;

        try
        {
            String array = " [ { \"Titulo\": \"Nombre del emisor\", \"Valor\": \"INS\" }, { \"Titulo\": \"Número de operación\", \"Valor\": \"893234\" }, { \"Titulo\": \"Monto\", \"Valor\": \"$23.923,93\" }, { \"Titulo\": \"Pignorado\", \"Valor\": \"Sí\" } ]";

            data = new JSONArray(array);

            LinearLayout layoutData = (LinearLayout) findViewById(R.id.layoutData);

            GeneralUtils.fntFillLayout(layoutData, data);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
