package com.novamobile.novacomp.insvalores.Entity;


public class CarteraEntity {

    private int IdCartera;
    private String Cartera;

    public int getIdCartera() {
        return IdCartera;
    }

    public void setIdCartera(int idCartera) {
        IdCartera = idCartera;
    }

    public String getCartera() {
        return Cartera;
    }

    public void setCartera(String cartera) {
        Cartera = cartera;
    }

}
