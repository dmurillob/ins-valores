package com.novamobile.novacomp.insvalores.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedPreferencesCustom {

    private String gNombre;
    private Object gValor;
    private Context gContext;
    private SharedPreferences gShared;

    public  SharedPreferencesCustom(Context pContext){
        this.gContext = pContext;

    }

    public void fntInitShared(String pNombre){
        gShared = gContext.getSharedPreferences(pNombre, Context.MODE_PRIVATE);
    }


    public String fntGetValue(String pKey,String pDefaultValue){
        return gShared.getString(pKey, pDefaultValue);
    }

    public void fntupdateValue(String pKey,String pValue){
        try{
            SharedPreferences.Editor editor = gShared.edit();
            editor.putString(pKey, pValue);
            editor.commit();
        }catch (Exception ex){
            Log.d("Error",ex.getMessage());
        }
    }

    public void fntDeleteReference(String pNombre){
        try{
            SharedPreferences.Editor editor = gShared.edit();
            editor.remove(pNombre);
            editor.apply();
        }catch (Exception ex){
            Log.d("Error",ex.getMessage());
        }
    }

    public void fntClenSharedReference(){
        gShared = null;
    }
}
