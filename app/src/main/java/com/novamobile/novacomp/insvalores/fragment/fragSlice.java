package com.novamobile.novacomp.insvalores.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.R;

public class fragSlice extends Fragment {

    private int index;

    public static fragSlice newInstance(int index) {

        // Instantiate a new fragment
        fragSlice fragment = new fragSlice();

        // Save the parameters
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);

        return fragment;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //super.onActivityCreated(savedInstanceState);

        this.index = (getArguments() != null) ? getArguments().getInt("index")  : -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.frag_slice,container, false);

        // Show the current page index in the view
        TextView tvIndex = (TextView) rootView.findViewById(R.id.tvIndex);
        tvIndex.setText(String.valueOf(this.index));

        // Change the background color
        rootView.setBackgroundColor(Color.RED);

        return rootView;

    }

}
