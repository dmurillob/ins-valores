package com.novamobile.novacomp.insvalores.Utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.novamobile.novacomp.insvalores.ContactoActivity;
import com.novamobile.novacomp.insvalores.MenuActivity;
import com.novamobile.novacomp.insvalores.R;

public class NavbarUtils extends AppCompatActivity {

    private ImageView mImgSalir,mImgMenu,mImgBack;

    public void fntNavbarfunctions(final Activity pActivity,boolean pMenu,boolean pBack){

        mImgSalir = (ImageView)pActivity.findViewById(R.id.imgSalir);

        mImgSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeneralUtils.fntShowDialogClose(pActivity,"IMPORTANTE","¿Desea cerrar la sesion en este momento?");
            }
        });

        if(pMenu){
            mImgMenu = (ImageView)pActivity.findViewById(R.id.imgMenu);
            mImgMenu.setVisibility(View.VISIBLE);
            mImgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intento = new Intent(pActivity, MenuActivity.class);
                    intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    pActivity.startActivity(intento);
                    pActivity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            });
        }

        if(pBack){
            mImgBack = (ImageView)pActivity.findViewById(R.id.imgBack);
            mImgBack.setVisibility(View.VISIBLE);
            mImgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pActivity.finish();
                }
            });
        }

    }


}
