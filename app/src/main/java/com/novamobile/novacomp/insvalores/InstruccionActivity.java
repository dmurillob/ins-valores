package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Utils.GeneralUtils;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import org.json.JSONArray;
import org.w3c.dom.Text;

/**
 * Created by Novacomp on 18/4/17.
 */

public class InstruccionActivity extends NavbarUtils {
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruccion);
        super.fntNavbarfunctions(this,true,true);

        mActivity = this;
        Intent intent = getIntent();

        TextView txtTitulo = (TextView)findViewById(R.id.lblInstruccion);
        txtTitulo.setText(intent.getStringExtra("Titulo"));

        JSONArray data = null;

        try
        {
            String array = "[ { \"Titulo\":\"Título\", \"Data\":[ { \"Nombre\":\"Código\", \"Valor\":\"CRG0000B89G0\" }, { \"Nombre\":\"Emisor\", \"Valor\":\"G\" }, { \"Nombre\":\"Instrumento\", \"Valor\":\"tp\" }, { \"Nombre\":\"Vencimiento\", \"Valor\":\"28/03/2018 12:00:00 AM\" }, { \"Nombre\":\"Perio\", \"Valor\":\"Semestral\" } ] }, { \"Titulo\":\"Información de la orden\", \"Data\":[ { \"Nombre\":\"Bolsa\", \"Valor\":\"Bolsa Nacional de Valores\" }, { \"Nombre\":\"Mercado\", \"Valor\":\"G\" }, { \"Nombre\":\"Moneda Instr\", \"Valor\":\"Colones\" }, { \"Nombre\":\"Moneda Liq\", \"Valor\":\"Dólares\" }, { \"Nombre\":\"F. Registro\", \"Valor\":\"24-03-2017\" }, { \"Nombre\":\"F. Vigencia\", \"Valor\":\"24-03-2017\" } ] }, { \"Titulo\":\"Información del título a comprar\", \"Data\":[ { \"Nombre\":\"Facial Mínimo\", \"Valor\":\"500000\" }, { \"Nombre\":\"Facial Máximo\", \"Valor\":\"750000\" }, { \"Nombre\":\"Transado\", \"Valor\":\"\" }, { \"Nombre\":\"Precio Mínimo\", \"Valor\":\"99\" }, { \"Nombre\":\"Precio Máximo\", \"Valor\":\"102\" }, { \"Nombre\":\"Rend. Mínimo\", \"Valor\":\"5\" }, { \"Nombre\":\"Rend. Máximo\", \"Valor\":\"7\" } ] } ]";

            data = new JSONArray(array);

            LinearLayout layoutData = (LinearLayout) findViewById(R.id.layoutData);

            GeneralUtils.fntFillLayout(layoutData, data);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (!intent.getBooleanExtra("Aprobada", false))
        {
            RelativeLayout layoutAprobada = (RelativeLayout) findViewById(R.id.layoutAprobada);

            layoutAprobada.setBackgroundResource(R.color.RojoVencimiento);

            ImageView imgAprobada = (ImageView) layoutAprobada.findViewById(R.id.imgAprobada);

            imgAprobada.setVisibility(View.GONE);

            TextView lblAprobada = (TextView) layoutAprobada.findViewById(R.id.lblAprobada);

            lblAprobada.setVisibility(View.GONE);

            Button btnAprobar = (Button) layoutAprobada.findViewById(R.id.btnAprobar);

            btnAprobar.setVisibility(View.VISIBLE);
        }
    }
}
