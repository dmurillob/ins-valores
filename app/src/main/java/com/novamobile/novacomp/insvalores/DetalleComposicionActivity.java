package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Utils.GeneralUtils;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class DetalleComposicionActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_composicion);
        super.fntNavbarfunctions(this,true,true);

        Intent intent = getIntent();
        mActivity = this;

        TextView txtTitulo = (TextView)findViewById(R.id.txtTitulo);
        txtTitulo.setText(intent.getStringExtra("Name"));

        JSONArray data = null;

        try
        {
            String array = "[ { \"CodIsin\":\"345445s\", \"NumOperacion\":\"16102734355\", \"Data\":[ { \"Nombre\":\"Instrumento\", \"Color\":\"009245\", \"Valor\":\"Bfi1c\" }, { \"Nombre\":\"Vencimiento\", \"Color\":\"f55e4e\", \"Valor\":\"01/01/2020\" }, { \"Nombre\":\"Precio mercado\", \"Color\":\"f2c574\", \"Valor\":\"86,74%\" }, { \"Nombre\":\"Rendimiento compra\", \"Color\":\"5e93c4\", \"Valor\":\"86,74%\" }, { \"Nombre\":\"Monto del cliente (₡)\", \"Color\":\"776b5f\", \"Valor\":\"889.103.450,00\" } ] }, { \"CodIsin\":\"345445s\", \"NumOperacion\":\"16102734356\", \"Data\":[ { \"Nombre\":\"Instrumento\", \"Color\":\"009245\", \"Valor\":\"Bfi1c\" }, { \"Nombre\":\"Vencimiento\", \"Color\":\"f55e4e\", \"Valor\":\"01/01/2020\" }, { \"Nombre\":\"Precio mercado\", \"Color\":\"f2c574\", \"Valor\":\"86,74%\" }, { \"Nombre\":\"Rendimiento compra\", \"Color\":\"5e93c4\", \"Valor\":\"86,74%\" }, { \"Nombre\":\"Monto del cliente (₡)\", \"Color\":\"776b5f\", \"Valor\":\"889.103.450,00\" } ] } ]";

            data = new JSONArray(array);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        LinearLayout layoutData = (LinearLayout) findViewById(R.id.layoutData);

        for (int i = 0; i < data.length(); i++)
        {
            RelativeLayout layoutCuadro = (RelativeLayout) getLayoutInflater().inflate(R.layout.layout_recuadro_gris, null);

            LinearLayout layoutDatos = (LinearLayout) layoutCuadro.getChildAt(0);

            RelativeLayout layoutTitulo = (RelativeLayout) getLayoutInflater().inflate(R.layout.celda_composicion_grafico, null);

            try
            {
                JSONObject obj = data.getJSONObject(i);

                TextView lblID = (TextView) layoutTitulo.findViewById(R.id.lblID);

                lblID.setText(obj.getString("NumOperacion"));

                Button btnGrafico = (Button) layoutTitulo.findViewById(R.id.btnGrafico);

                btnGrafico.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         Intent intento = new Intent(mActivity, GraficoPrecioMoneda.class);
                         mActivity.startActivity(intento);
                         mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
                     }
                });

                layoutDatos.addView(layoutTitulo);

                GeneralUtils.fntFillLayout(layoutDatos, obj, true);

                if (i < data.length() - 1)
                {
                    int marginInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());

                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, marginInDp);
                    layoutCuadro.setLayoutParams(lp);
                }

                layoutData.addView(layoutCuadro);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
