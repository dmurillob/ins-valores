package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

public class MenuActivity extends NavbarUtils {

    private ConstraintLayout mLayoutCartera,mLayoutBoletin,mLayoutCalendario,mLayoutChat,mLayoutInstrucciones;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        super.fntNavbarfunctions(this,false,false);

        mActivity = this;
        mLayoutCartera = (ConstraintLayout)findViewById(R.id.layoutCartera);
        mLayoutBoletin = (ConstraintLayout)findViewById(R.id.layoutBoletin);
        mLayoutCalendario  = (ConstraintLayout)findViewById(R.id.layoutCalendario);
        mLayoutChat  = (ConstraintLayout)findViewById(R.id.layoutChat);
        mLayoutInstrucciones = (ConstraintLayout)findViewById(R.id.layoutInstruccion);

        mLayoutCartera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, ListaCarteraActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

        mLayoutBoletin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, ListaBoletinActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

        mLayoutCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, CalendarioActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

        mLayoutChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, ChatActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

        mLayoutInstrucciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, ListaInstruccionesActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

    }
}
