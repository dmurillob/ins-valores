package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

public class MenuComposicionActivity extends NavbarUtils {


    private ConstraintLayout mLayoutMercados,mLayoutMoneda,mLayoutEmisor,mLayoutPlazo,mLayoutInstrumento,mLayoutCalificacion;

    private boolean mFlagBack;
    private Activity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_composicion);
        super.fntNavbarfunctions(this,true,true);

        mActivity = this;

        mLayoutMercados = (ConstraintLayout)findViewById(R.id.layoutMercados);
        mLayoutMoneda = (ConstraintLayout)findViewById(R.id.layoutMoneda);
        mLayoutEmisor = (ConstraintLayout)findViewById(R.id.layoutEmison);
        mLayoutPlazo = (ConstraintLayout)findViewById(R.id.layoutPlazo);
        mLayoutInstrumento = (ConstraintLayout)findViewById(R.id.layoutInstruccion);
        mLayoutCalificacion = (ConstraintLayout)findViewById(R.id.layoutCalificacion);

        mLayoutMercados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",0);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });

        mLayoutCalificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",1);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });


        mLayoutMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",2);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });


        mLayoutEmisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",3);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });

        mLayoutPlazo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",4);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });

        /*
        mLayoutInstrumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, GraficosActivity.class);
                intento.putExtra("Posicion",4);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.activity_slide_out_left,R.anim.activity_slide_in_right);
            }
        });
        */




    }
}
