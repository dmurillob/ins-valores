package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.novamobile.novacomp.insvalores.Utils.GeneralUtils;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import org.json.JSONArray;

import java.util.ArrayList;

public class PortafolioActivity extends NavbarUtils {

    private Activity mActivity;
    private ArrayList<String> ListArray = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portafolio);
        super.fntNavbarfunctions(this,true,true);

        mActivity = this;
        Intent intent = getIntent();

        TextView txtTitulo = (TextView)findViewById(R.id.txtNombrePortafolio);
        txtTitulo.setText(intent.getStringExtra("Titulo"));

        fntChart();

        Button btnComposicion = (Button)findViewById(R.id.btnGraficos);
        btnComposicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(mActivity, MenuComposicionActivity.class);
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        });

        JSONArray data = null;
        LinearLayout layoutData = null;

        try
        {
            String array = "[ { \"Jerarquia\":1, \"Titulo\":\"Activos\", \"Data\":[ { \"Jerarquia\":2, \"Titulo\":\"Renta variable\", \"Data\":[ { \"Jerarquia\":3, \"Nombre\":\"Dólares\", \"Valor\":\"$452,910.00\" } ] }, { \"Jerarquia\":2, \"Titulo\":\"Renta fija\", \"Data\":[ { \"Jerarquia\":3, \"Nombre\":\"Colones\", \"Valor\":\"₡452,910.00\" }, { \"Jerarquia\":3, \"Nombre\":\"Dólares\", \"Valor\":\"$452,910.00\" }, { \"Jerarquia\":3, \"Nombre\":\"Unidades de desarrollo\", \"Valor\":\"2,430.78\" } ] }, { \"Jerarquia\":2, \"Titulo\":\"Fondos de inversión financieros\", \"Data\":[ { \"Jerarquia\":3, \"Nombre\":\"Colones\", \"Valor\":\"₡452,910.00\" }, { \"Jerarquia\":3, \"Nombre\":\"Dólares\", \"Valor\":\"$452,910.00\" } ] } ], \"Total\":[ { \"Nombre\":\"Total Activos Colonizado\", \"Valor\":\"₡76,224,299,781.38\" } ] }, { \"Jerarquia\":1, \"Titulo\":\"Pasivos\", \"Data\":[ { \"Jerarquia\":2, \"Titulo\":\"\", \"Data\":[ { \"Jerarquia\":3, \"Nombre\":\"Dólares\", \"Valor\":\"$452,910.00\" } ] } ], \"Total\":[ { \"Nombre\":\"Total Pasivos Colonizado\", \"Valor\":\"₡76,224,299,781.38\" } ] } ]";

            data = new JSONArray(array);

            layoutData = (LinearLayout) findViewById(R.id.layoutDataInterno);

            GeneralUtils.fntFillLayout(layoutData, data, "45bbff");

            array = "[ { \"Titulo\":\"Riesgo de Mercado\", \"Data\":[ { \"Nombre\":\"Duración\", \"Valor\":\"2,51\" }, { \"Nombre\":\"Posición neta en moneda extranjera\", \"Valor\":\"$27.903.133,24\" } ] }, { \"Titulo\":\"Riesgo de liquidez\", \"Data\":[ { \"Nombre\":\"Porcentaje del valor de mercado de la cartera en efectivo disponible\", \"Valor\":\"8.36%\" }, { \"Nombre\":\"Razón de apalancamiento\", \"Valor\":\"0,03\" } ] } ]";

            data = new JSONArray(array);

            layoutData = (LinearLayout) findViewById(R.id.layoutDataExterno);

            GeneralUtils.fntFillLayout(layoutData, data);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    private void fntChart(){

        LineChart lineChart = (LineChart) findViewById(R.id.chart);


        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(1, 1,  "20/05"));
        entries.add(new Entry(2, 15, "21/05"));
        entries.add(new Entry(3, 20, "22/05"));
        entries.add(new Entry(4, 17, "23/05"));
        entries.add(new Entry(5, 17, "24/05"));
        entries.add(new Entry(6, 5,  "25/05"));
        entries.add(new Entry(7, 17, "26/05"));
        entries.add(new Entry(8, 17, "27/05"));
        entries.add(new Entry(9, 9,  "28/05"));


        ListArray.add("20/05");
        ListArray.add("21/05");
        ListArray.add("22/05");
        ListArray.add("23/05");
        ListArray.add("24/05");
        ListArray.add("25/05");
        ListArray.add("26/05");
        ListArray.add("27/05");
        ListArray.add("28/05");

        LineDataSet dataset = new LineDataSet(entries, "PUNTOS");

        XAxis xval = lineChart.getXAxis();
        xval.setDrawLabels(true);
        xval.setValueFormatter(new IAxisValueFormatter(){
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Log.i("zain", "value " + value);
                return ListArray.get(Math.round(value)-1);
            }
        });

        //dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        dataset.setDrawFilled(true);
        dataset.setValueTextSize(10);
        dataset.setCircleRadius(4f);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataset);

        LineData data = new LineData(dataSets);

        lineChart.setData(data);
        //lineChart.animateY(5000);


        lineChart.setTouchEnabled(false);

        // enable scaling and dragging
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.getDescription().setEnabled(true);
        //lineChart.setScaleXEnabled(true);
        //lineChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(5f, "AXIS X");
        llXAxis.setLineWidth(2f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);
        llXAxis.setLineColor(Color.GRAY);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.addLimitLine(llXAxis);
    }


}
