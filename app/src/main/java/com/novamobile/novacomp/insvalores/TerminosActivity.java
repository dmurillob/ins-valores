package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.novamobile.novacomp.insvalores.Utils.SharedPreferencesCustom;

import static android.view.View.GONE;

public class TerminosActivity extends AppCompatActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminos);
        mActivity = this;


        LinearLayout layoutChecbox = (LinearLayout)findViewById(R.id.layoutChecbox);
        LinearLayout layoutBoton = (LinearLayout)findViewById(R.id.layoutBoton);

        final SharedPreferencesCustom objShared = new SharedPreferencesCustom(getApplicationContext());
        objShared.fntInitShared("TerminoCondiciones");

        if(objShared.fntGetValue("Aceptar","NO").equals("OK")) {
            layoutChecbox.setVisibility(View.GONE);
            layoutBoton.setVisibility(View.VISIBLE);

            Button btnEntendido = (Button)findViewById(R.id.btnEntendido);
            btnEntendido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intento = new Intent(mActivity, PublicActivity.class);
                    intento.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intento.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mActivity.startActivity(intento);
                    mActivity.overridePendingTransition(R.anim.activity_slide_out_right, R.anim.activity_slide_in_left);
                }
            });

        }else{
            layoutChecbox.setVisibility(View.VISIBLE);
            layoutBoton.setVisibility(GONE);

            CheckBox chkAceptar = (CheckBox)findViewById(R.id.chkAceptar);
            chkAceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    objShared.fntupdateValue("Aceptar","OK");

                    Intent intent = new Intent(mActivity,TutorialActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                }
            });


        }




    }
}
