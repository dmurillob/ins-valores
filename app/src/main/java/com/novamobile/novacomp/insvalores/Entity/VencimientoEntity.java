package com.novamobile.novacomp.insvalores.Entity;

public class VencimientoEntity {

    private int mIdVencimiento;
    private String mVencimientoDesc;
    private String mMonto;

    public String getmMonto() {
        return mMonto;
    }

    public void setmMonto(String mMonto) {
        this.mMonto = mMonto;
    }

    public int getmIdVencimiento() {
        return mIdVencimiento;
    }

    public void setmIdVencimiento(int mIdVencimiento) {
        this.mIdVencimiento = mIdVencimiento;
    }

    public String getmVencimientoDesc() {
        return mVencimientoDesc;
    }

    public void setmVencimientoDesc(String mVencimientoDesc) {
        this.mVencimientoDesc = mVencimientoDesc;
    }

}
