package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.novamobile.novacomp.insvalores.Adapter.AdapterListaInstrucciones;
import com.novamobile.novacomp.insvalores.Entity.InstruccionEntity;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.util.ArrayList;

public class ListaInstruccionesActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_instrucciones);
        super.fntNavbarfunctions(this,false,true);

        mActivity = this;

        ArrayList<InstruccionEntity> ListEntity = new ArrayList<>();

        for (int i = 1 ; i <= 5 ; i++)
        {
            InstruccionEntity objEntity = new InstruccionEntity(i, "Instruccion " + String.valueOf(i), i == 2);

            ListEntity.add(objEntity);
        }

        ListAdapter adapter = new AdapterListaInstrucciones(getApplicationContext(),R.layout.celda_instruccion,ListEntity,mActivity);
        ListView Lista = (ListView)findViewById(R.id.ListInstrucciones);
        Lista.setAdapter(adapter);
    }
}
