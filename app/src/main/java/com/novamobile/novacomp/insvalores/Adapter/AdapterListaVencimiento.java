package com.novamobile.novacomp.insvalores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.DetalleComposicionActivity;
import com.novamobile.novacomp.insvalores.DetalleVencimientoActivity;
import com.novamobile.novacomp.insvalores.Entity.CarteraEntity;
import com.novamobile.novacomp.insvalores.Entity.VencimientoEntity;
import com.novamobile.novacomp.insvalores.PortafolioActivity;
import com.novamobile.novacomp.insvalores.R;
import java.util.List;


public class AdapterListaVencimiento extends ArrayAdapter<VencimientoEntity> {

    Context mContext;
    Activity mActivity;
    List<VencimientoEntity> mListItems;

    public AdapterListaVencimiento(Context context, int resource) {
        super(context, resource);
    }

    public AdapterListaVencimiento(Context context, int resource, List<VencimientoEntity> items, Activity activity) {
        super(context, resource, items);
        mContext=context;
        mListItems = items;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_vencimiento, null);
        }

        final VencimientoEntity objEntity = mListItems.get(position);
        final TextView txtVencimiento = (TextView)convertView.findViewById(R.id.txtVencimiento);
        txtVencimiento.setText(objEntity.getmVencimientoDesc());

        final LinearLayout eventsLL = (LinearLayout) convertView.findViewById(R.id.layoutVencimiento);
        eventsLL.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                Intent intento = new Intent(mActivity, DetalleVencimientoActivity.class);
                intento.putExtra("Titulo", objEntity.getmVencimientoDesc());
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }

        });

        return convertView;
    }

}
