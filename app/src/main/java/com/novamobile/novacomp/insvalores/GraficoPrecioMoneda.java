package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.util.ArrayList;

public class GraficoPrecioMoneda extends NavbarUtils {

    private Activity mActivity;
    private ArrayList<String> ListArray = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico_precio_moneda);
        super.fntNavbarfunctions(this,true,true);

        mActivity = this;
        fntChart();
    }


    private void fntChart(){

        LineChart lineChart = (LineChart) findViewById(R.id.chart);


        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(1, 1));
        entries.add(new Entry(2, 15));
        entries.add(new Entry(3, 20));
        entries.add(new Entry(4, 17));
        entries.add(new Entry(5, 17));
        entries.add(new Entry(6, 5));
        entries.add(new Entry(7, 17));
        entries.add(new Entry(8, 17));
        entries.add(new Entry(9, 9));


        ListArray.add("20/05");
        ListArray.add("21/05");
        ListArray.add("22/05");
        ListArray.add("23/05");
        ListArray.add("24/05");
        ListArray.add("25/05");
        ListArray.add("26/05");
        ListArray.add("27/05");
        ListArray.add("28/05");



        XAxis xval = lineChart.getXAxis();
        xval.setDrawLabels(true);
        xval.setValueFormatter(new IAxisValueFormatter(){
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Log.i("zain", "value " + value);
                return ListArray.get(Math.round(value)-1);
            }
        });

        LineDataSet dataset = new LineDataSet(entries, "PUNTOS");
        //dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        dataset.setDrawFilled(true);
        dataset.setValueTextSize(10);
        dataset.setCircleRadius(4f);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataset);

        LineData data = new LineData(dataSets);

        lineChart.setData(data);
        //lineChart.animateY(5000);


        lineChart.setTouchEnabled(false);

        // enable scaling and dragging
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.getDescription().setEnabled(false);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);
    }
}
