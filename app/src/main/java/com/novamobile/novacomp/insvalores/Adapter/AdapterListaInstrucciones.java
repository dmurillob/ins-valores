package com.novamobile.novacomp.insvalores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Entity.InstruccionEntity;
import com.novamobile.novacomp.insvalores.InstruccionActivity;
import com.novamobile.novacomp.insvalores.R;

import java.util.List;

public class AdapterListaInstrucciones extends ArrayAdapter<InstruccionEntity> {
    Context mContext;
    Activity mActivity;
    List<InstruccionEntity> mListItems;

    public AdapterListaInstrucciones(Context context, int resource) {
        super(context, resource);
    }

    public AdapterListaInstrucciones(Context context, int resource, List<InstruccionEntity> items, Activity activity) {
        super(context, resource, items);
        mContext=context;
        mListItems = items;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.celda_instruccion, null);
        }

        final InstruccionEntity objEntity = mListItems.get(position);

        final TextView txtTitulo = (TextView)convertView.findViewById(R.id.lblInstruccion);
        txtTitulo.setText(objEntity.getInstruccion());

        TextView lblInstruccion = (TextView) convertView.findViewById(R.id.lblInstruccion);

        lblInstruccion.setText(objEntity.getInstruccion());

        ImageView imgAprobada = (ImageView) convertView.findViewById(R.id.imgAprobada);

        if (objEntity.getAprobada())
        {
            imgAprobada.setImageResource(R.drawable.ic_instruccion_verde);
        }
        else
        {
            imgAprobada.setImageResource(R.drawable.ic_instruccion_rojo);
        }

        final RelativeLayout eventsLL = (RelativeLayout) convertView.findViewById(R.id.layoutInstruccion);
        eventsLL.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                Intent intento = new Intent(mActivity, InstruccionActivity.class);
                intento.putExtra("Titulo", objEntity.getInstruccion());
                intento.putExtra("Aprobada", objEntity.getAprobada());
                mActivity.startActivity(intento);
                mActivity.overridePendingTransition(R.anim.in, R.anim.activity_out);
            }

        });

        return convertView;
    }
}
