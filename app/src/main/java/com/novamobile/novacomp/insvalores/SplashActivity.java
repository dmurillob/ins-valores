package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.novamobile.novacomp.insvalores.Utils.SharedPreferencesCustom;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Activity ThisActivity = this;

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                Intent intent = null;

                final SharedPreferencesCustom objShared = new SharedPreferencesCustom(getApplicationContext());
                objShared.fntInitShared("TerminoCondiciones");

                if(objShared.fntGetValue("Aceptar","NO").equals("OK")) {
                    intent = new Intent(ThisActivity, PublicActivity.class);
                }else{
                    intent = new Intent(ThisActivity, TerminosActivity.class);
                }

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.in, R.anim.activity_out);
            }
        }, 3500); // 2 seconds
    }
}
