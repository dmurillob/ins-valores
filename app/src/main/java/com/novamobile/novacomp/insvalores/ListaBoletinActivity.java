package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.novamobile.novacomp.insvalores.Adapter.AdapterListaBoletin;
import com.novamobile.novacomp.insvalores.Adapter.AdapterListaCartera;
import com.novamobile.novacomp.insvalores.Entity.CarteraEntity;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;

import java.util.ArrayList;

public class ListaBoletinActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_boletin);
        super.fntNavbarfunctions(this,false,true);

        mActivity = this;

        TextView txtTitulo = (TextView)findViewById(R.id.txtNombre);
        txtTitulo.setText("Boletines");

        ArrayList<String> ListEntity = new ArrayList<String>();

        ListEntity.add("Mercado");
        ListEntity.add("Semanal");
        ListEntity.add("Mensual");


        ListAdapter adapter = new AdapterListaBoletin(getApplicationContext(),android.R.layout.simple_list_item_1,ListEntity,mActivity);
        ListView Lista = (ListView)findViewById(R.id.ListBoletin);
        Lista.setAdapter(adapter);
    }
}
