package com.novamobile.novacomp.insvalores.Entity;

public class InstruccionEntity {
    private int IdInstruccion;
    private String Instruccion;
    private boolean aprobada;

    public InstruccionEntity(int idInstruccion, String instruccion, boolean aprobada) {
        this.IdInstruccion = idInstruccion;
        this.Instruccion = instruccion;
        this.aprobada = aprobada;
    }

    public void setAprobada(boolean aprobada) {
        this.aprobada = aprobada;
    }

    public int getIdInstruccion() {
        return IdInstruccion;
    }

    public String getInstruccion() {
        return Instruccion;
    }

    public boolean getAprobada() {
        return aprobada;
    }
}

