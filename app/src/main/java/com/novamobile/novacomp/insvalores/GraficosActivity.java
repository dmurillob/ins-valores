package com.novamobile.novacomp.insvalores;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.novamobile.novacomp.insvalores.Adapter.CustomFragmentPagerAdapter;
import com.novamobile.novacomp.insvalores.Utils.NavbarUtils;
import com.novamobile.novacomp.insvalores.Utils.ProgressDotWidget;
import com.novamobile.novacomp.insvalores.fragment.fragSlice;
import com.novamobile.novacomp.insvalores.fragment.fragSliceChart;

public class GraficosActivity extends NavbarUtils {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficos);
        super.fntNavbarfunctions(this,true,true);
        mActivity = this;

        Intent intent = getIntent();


        int PositionPager = intent.getIntExtra("Posicion",0);


        ViewPager pager = (ViewPager) this.findViewById(R.id.pager);
        final ProgressDotWidget objDot = (ProgressDotWidget)findViewById(R.id.progress_dots);

        objDot.setDotCount(6);

        // Create an adapter with the fragments we show on the ViewPager
        CustomFragmentPagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(fragSliceChart.newInstance(1,"Composición por mercados") );
        adapter.addFragment(fragSliceChart.newInstance(2,"Composición por calificación") );
        adapter.addFragment(fragSliceChart.newInstance(3,"Composición por moneda") );
        adapter.addFragment(fragSliceChart.newInstance(4,"Composición por emisor") );
        adapter.addFragment(fragSliceChart.newInstance(5,"Composición por plazo") );
        adapter.addFragment(fragSliceChart.newInstance(6,"Composición por instrumento") );

        pager.setAdapter(adapter);
        pager.setCurrentItem(PositionPager);
        objDot.setActivePosition(PositionPager);

        if(PositionPager>1){
            objDot.fntMoveSelector(PositionPager);
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                objDot.setActivePosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // objDot.setActivePosition(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               // objDot.setActivePosition(position);
            }
        });



    }
}
